# Problem statement

A hospital has $D$ doctors who can perform $P$ different procedures. Every day, each doctor records the number
of minutes they worked, and how many times they performed each of the $P$ procedure types, generating a work unit
record of the form (_docid_, _time_, _counts_). For example, if $P = 3$, the work unit record _(3, 510, 
\[10, 0, 3\])_ means that doctor 3 worked 510 min (8.5 hours), and performed procedure 1 a total of 10 times, 
procedure 3 a total of 3 times, and did not perform procedure 2 at all.

Given $N$ work unit records, the goal is to infer how long each doctor takes to perform each type of procedure


# Bayesian model

I assume that the time doctor $i$ takes to perform procedure $j$ is normally distributed with mean
$\mu_{ij}$ and standard deviation $\sigma_{ij}$:
\begin{displaymath}
  t_{ij} \sim \mathcal{N}(\mu_{ij}, \sigma_{ij})
\end{displaymath}
Then, the total duration of a work unit in which doctor $i$ performed each procedure $n_{ij}$ times, for
$j$ running from 1 to $P$, is also a normally distributed random variable
\begin{align*}
t &\sim \mathcal{N}(\mu_i^{\mathrm{eff}}, \sigma_i^{\mathrm{eff}}) \\
\mu_i^{\mathrm{eff}} &= \sum_{j = 1}^P n_{ij} \mu_{ij} \\
\sigma_i^{\mathrm{eff}} &= \sqrt{\sum_{j = 1}^P n_{ij} \sigma_{ij}^2}.
\end{align*}

For each doctor, the vector of means $\bar\mu_i = (\mu_{i1}, \dots, \mu_{iP})$ is also a random variable
following a multivariate normal distribution with mean $\bar\mu_0$ and covariance matrix $\Sigma_0$:
\begin{displaymath}
  \bar\mu_i \sim \mathcal{MN}(\bar\mu_0, \Sigma_0) 
\end{displaymath}
I factorize the covariance matrix as $\Sigma_0 = D(\bar\sigma^2_0) \cdot \Omega_0$, where $D(\bar{x})$ is a diagonal
matrix having the vector $\bar{x}$ as its diagonal, and $\Omega_0$ is a correlation matrix, i.e a square, positive
definite, symmetric matrix, with 1 on the diagonal, and off-diagonal entries between -1 and 1.

I choose to model the distribution of individual doctor skill as a multivariate normal variable, in order to
capture the fact that procedures which require very similar sets of skills might have highly correlated
execution times.

I place weakly informative Cauchy priors on $\bar\mu_0$, $\bar\sigma_0$, and all the individual 
$\sigma_{ij}$ parameters:
\begin{align*}
\bar\mu_0 & \sim \mathcal{C}(0, \eta) \\
\bar\sigma_0 & \sim \mathcal{C}(0, \eta) \\
\sigma_{ij} & \sim \mathcal{C}(0, \eta),
\end{align*}
where $\eta$ is a regularization parameter, provided as an input by the user.

Finally, I place a weakly informative Lewandowski-Kurowicka-Joe prior on the correlation matrix $\Omega_0$ 
(see [1] for details)
\begin{displaymath}
\Omega_0 \sim \mathcal{LKJ}(\nu),
\end{displaymath}
where $\nu$ is also a user provided regularization parameter.

The file `hospital.stan` contains a sampler for the posterior distribution of this model, implemented
using the probabilistic programming language Stan

# Synthetic data

In order to test the Bayesian inference model, I wrote a hospital simulator,
which generates random work unit data, according to the model described in the
previous section. The inputs to this simulator are $\bar\mu_0$, $\Sigma_0$, the
number of doctors, $D$, and the regularization parameters $\nu$ and $\eta$.
Once initialized, a simulator can be used to generate a synthetic data set of
work units, which are then fed to the inference engine. The ground truth and
the result of the Bayesian inference are then displayed side by side, in order
to assess how well the inference engine succeeds in recovering the ground
truth. The file `hospital.py` implements the simulator, and, when run as a
script, will produce the side by side comparison.

# Installation and usage instructions

1. Create a virtual environment using `virtualenv <env_dir>`.
2. Activate the virtual environment: `. <env_dir>/bin/activate`.
3. Install the requirements: `pip install -r requirements.txt`.
4. Run the script: `./hospital.py`.
5. Wait for approximately 10 minutes (at least that's the running time on my laptop).

# References

1. Lewandowski, D., Kurowicka, D., Joe, H. _Generating random correlation matrices based on vines and 
   extended onion method_, JMVA, 100 (2009) pp. 1989--2001
