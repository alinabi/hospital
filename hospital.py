#!/usr/bin/env python3

import math
import sys
import numpy as np
import numpy.random as rnd
import tabulate as tbl
import pystan as stan


def indent(txt, level=1):
  txt = txt.split(sep='\n')
  txt = '\n'.join(('\t' * level) + line for line in txt)
  return txt


def section(title, body):
  return "\n\n".join([title, indent(body)])


class Doctor(object):

  def __init__(self, mu, sigma):
    if len(mu) != len(sigma):
      raise RuntimeError("mu and sigma parameters must have the same length")
    self._skills = list(zip(mu, sigma))

  def __len__(self):
    return len(self._skills)

  def __str__(self):
    txt = ("{} ± {}".format(mu, sigma) for (mu, sigma) in self._skills)
    txt = ", ".join(txt)
    txt = "Doctor[{}]".format(txt)
    return txt

  def do_task(self):
    i = rnd.choice(len(self._skills), replace=True)
    mu, sigma = self._skills[i]
    time = max(0, rnd.normal(mu, sigma))
    return (i, time)

  def do_work_unit(self):
    ns = [0] * len(self)  
    tmin = rnd.uniform(8*60, 12*60)
    time = 0
    while time < tmin:
      i, t = self.do_task()
      ns[i] = ns[i] + 1
      time = time + t
    return (round(time), ns)  

  def do_work(self, size=None):
    if size is None:
      return self.do_work_unit()
    return [self.do_work_unit() for i in range(size)]



class Hospital(object):

  def __init__(self, mean, cov, docs, sharpe=0.25):
    self._mean = np.array(mean)
    self._cov = np.array(cov)
    self._sharpe = sharpe
    ds = rnd.multivariate_normal(mean, cov, docs)
    ds = ((ms, [abs(rnd.normal(sharpe * m , sharpe * m / 3)) for m in ms]) for ms in ds)
    self._docs = [Doctor(m, s) for (m, s) in ds]

  def __len__(self):
    return len(self._docs)

  def _sigma(self):
    return np.sqrt(np.diag(self._cov))

  def _corr(self):
    n = len(self)
    tau = np.diag(1.0 / self._sigma())
    corr = tau.dot(self._cov).dot(tau)
    return corr

  def __str__(self):
    skills = zip(self._mean, self._sigma())
    skills = ["{0:.3f} ± {1:.3f}".format(m, s) for (m, s) in skills]
    procs = ["Procedure {}".format(i + 1) for i in range(len(skills))]
    skills = tbl.tabulate([skills], headers=procs, tablefmt="fancy_grid")
    skills = section("Skill distribution:", skills)
    corr = tbl.tabulate(self._corr(), tablefmt="fancy_grid")
    corr = section("Correlation matrix:", str(corr))
    docs = enumerate(self._docs, 1)
    docs = ([str(i)] + ['{0:.3f} ± {1:.3f}'.format(m, s) for (m, s) in d._skills]  for (i, d) in docs)
    docs = tbl.tabulate(docs, headers=(["Doctor"] + procs), tablefmt="fancy_grid")
    docs = section("Doctors:", docs) 
    body = "\n\n".join([skills, corr, docs])
    return section("Hospital ground truth:", body)

  def do_work(self, size, stan=False):
    res = (rnd.choice(len(self._docs)) for i in range(size))
    res = ((i + 1, self._docs[i]) for i in res)
    res = ((i, d.do_work_unit()) for (i, d) in res)
    res = ((doc, time, work) for (doc, (time, work)) in res)
    if not stan:
        return res
    d, t, w = zip(*res)
    data = { 'N': size, 'D': len(self), 'P': len(self._mean), 't': t, 'd': d, 'w': w}
    return data



class Result(object):

  def __init__(self, doc, corr):
    N, D, P = doc.shape
    I, J, K = corr.shape
    
    if I != N:
      raise RuntimeError("all params must have the same sample size")
    if J != K:
      raise RuntimeError("correlation matrix must be square")
    if J != P:
      raise RuntimeError("number of procedures must be the same for all parameters")
    
    self._N = N
    self._D = D
    self._P = P
    self._doc_mu = np.mean(doc, axis=0)
    self._doc_sigma = np.std(doc, axis=0)
    self._corr_mu = np.mean(corr, axis=0)
    self._corr_sigma = np.std(corr, axis=0)

  def __str__(self):
    hdrs = ["Doctor"] + ["Procedure {}".format(i + 1) for i in range(self._P)]
    docs = ((i + 1, zip(self._doc_mu[i, :], self._doc_sigma[i, :])) for i in range(self._D))  
    docs = ([str(i)] + ["{0:.3f} ± {1:.3f}".format(mu, sigma) for (mu, sigma) in data] for (i, data) in docs) 
    docs = section("Doctors:", tbl.tabulate(docs, headers=hdrs, tablefmt="fancy_grid"))
    corr = [["{0:.3f} ± {1:.3f}".format(self._corr_mu[i, j], self._corr_sigma[i, j]) for j in range(self._P)] for i in range(self._P)]
    corr = section("Correlation matrix:", tbl.tabulate(corr, tablefmt="fancy_grid"))
    body = "\n\n".join([corr, docs])
    return section("Inference results:", body) 



def infer(data, eta=1.0, nu=3.0, warmup=1000, samples=1000, **kwargs):
  data['eta'] = eta
  data['nu'] = nu
  kwargs['warmup'] = warmup
  kwargs['iter'] = warmup + samples
  fit = stan.stan(file='hospital.stan', data=data, **kwargs)
  print(fit, file=sys.stderr)
  post = fit.extract(pars=['doc', 'Corr'])
  doc = post['doc']
  corr = post['Corr']
  print('doc shape: {0}, corr shape: {1}'.format(doc.shape, corr.shape), file=sys.stderr)
  return Result(doc, corr)



if __name__ == '__main__':
  print("Instantiating hospital work simulator", file=sys.stderr)
  truth = Hospital([23, 12, 5], [[5.5, 1.2, 0.1], [1.2, 3.1, 0.1], [0.1, 0.1, 2.1]], 10, sharpe=0.1)
  print("Generating 2000 synthetic work units", file=sys.stderr)
  work = truth.do_work(2000, stan=True)
  print("Performing Bayesian inference", file=sys.stderr)
  res = infer(work)
  print("{0}\n\n{1}".format(truth, res))
