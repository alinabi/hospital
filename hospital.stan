data {
  real<lower=0> eta;          // parameter for standard deviation prior
  real<lower=1> nu;           // regularization parameter for skill correlation matrix           
  int<lower=1> N;             // number of work units
  int<lower=1> D;             // number of doctors
  int<lower=1> P;             // number of procedures
  int<lower=1, upper=D> d[N]; // doctor who performed each work unit
  int<lower=0> w[N, P];       // work performed in each work unit
  real<lower=0> t[N];         // duration of each work unit (in minutes)
}



transformed data {
  vector[P] x[N];

  for (i in 1:N) {
    x[i] = to_vector(w[i, :]);
  }
}



parameters {

  vector<lower=0>[P] mu0;
  vector<lower=0>[P] sigma0;
  cholesky_factor_corr[P] Omega;
  vector<lower=0>[P] mu[D];
  vector<lower=0>[P] sigma[D];

}



model {

  sigma0 ~ cauchy(0, eta);
  mu0 ~ cauchy(0, eta);
  Omega ~ lkj_corr_cholesky(nu); 

  for (i in 1:D) {
    mu[i] ~ multi_normal_cholesky(mu0, diag_pre_multiply(sigma0, Omega));
    sigma[i] ~ cauchy(0, eta);
  }

  for (i in 1:N) {
    real mu_eff;
    real var_eff;
    
    mu_eff = dot_product(x[i], mu[d[i]]);
    var_eff = dot_product(x[i], sigma[d[i]] .* sigma[d[i]]);
    t[i] ~ normal(mu_eff, sqrt(var_eff));
  }

}


generated quantities {

  corr_matrix[P] Corr;
  vector[P] doc[D];

  for (i in 1:D) {
    for (j in 1:P) {
      doc[i][j] = normal_rng(mu[i][j], sigma[i][j]);
    }
  }

  Corr = multiply_lower_tri_self_transpose(Omega);

}
